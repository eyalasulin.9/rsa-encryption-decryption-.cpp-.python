// Created by Eyal Asulin™
#include <iostream>
#include "rsa.h"

// For Testing Purpose

int main(void)
{
	RSA rsa;

    std::string s = "bla bla bla";
    std::cout << "String: " << s << std::endl;

	int *enc = RSA::encrypt(rsa.getPublicKey(), s);

	std::cout << "Encrypted: ";
	for (int i = 0; enc[i] != -1; i++)
	{
		std::cout << enc[i] << ", ";
	}
	std::cout << std::endl;
    
	std::cout << "Decrypted: " << rsa.decrypt(enc) << std::endl;

	system("PAUSE");
	return 0;
}